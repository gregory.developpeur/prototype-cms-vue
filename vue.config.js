/* eslint-disable semi */
/* eslint-disable quotes */
// console.log('process.env.NODE_ENV =', process.env.NODE_ENV)
module.exports = {
  publicPath: process.env.VUE_APP_ROOT_API,

  pages: {
    index: {
      entry: "src/main.js",
      template: "public/index.html",
      filename: "index.html",
      // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
      title: " Public page",
      chunks: ["chunk-vendors", "chunk-common", "index"]
    },
    admin: {
      entry: "src/main-admin.js",
      template: "public/index-admin.html",
      filename: "index-admin.html",
      // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
      title: " Admin page",
      chunks: ["chunk-vendors-admin", "chunk-common-admin", "admin"]
    }    
  },
  css: {
    loaderOptions: {
      // sass: {
      //   data: `@import "@/scss/_variables.scss";`
      // }
    }
  }
};
