/* eslint-disable space-before-function-paren */
/* eslint-disable semi */
/* eslint-disable quotes */
import Vue from "vue";
import Vuex from "vuex";
import userStore from "./store/Users/userStore";
import cartStore from "./store/Cart/cartStore";
import clientStore from "./store/Client/clientStore";

Vue.use(Vuex);
const debug = process.env.NODE_ENV !== "production";
export default new Vuex.Store({
  modules: { userStore, cartStore, clientStore },
  strict: debug
  // state: {},
  // mutations: {},
  // actions: {}
});
