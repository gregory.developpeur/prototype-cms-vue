/* eslint-disable semi */
/* eslint-disable quotes */
import Vue from "vue";
import Router from "vue-router";
import VueMeta from 'vue-meta'
import { routesFromBuilder } from "@/config-data/routes.js";

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routesFromBuilder
});

router.beforeEach((to, from, next) => {
  // tricks pour appeler vuetify spécifiquement sur route admin car sinon il intérfere avec css front
  to.matched.forEach(routMatched => {
    if (routMatched.path === "/admin" || routMatched.path === "/login") {
      require("vuetify/dist/vuetify.min.css");
    }
  });

  if (!to.matched.length) {
    next("/404");
  } else {
    next();
  }

  // controle si admin est authentifié
  // todo vérif role / permission
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem("authUser"));
    if (authUser && authUser.access_token) {
      next();
    } else {
      next({ name: "login" });
    }
  }
  next();
});

Vue.use(Router);
Vue.use(VueMeta);

export default router;
