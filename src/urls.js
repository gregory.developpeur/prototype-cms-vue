/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable semi */
const URL_API = process.env.VUE_APP_URL_DISTANT_API;

export const urls = {
  // routes des diaporamas
  URL_DIAPO_ACTIV: URL_API + "api/diaporamas/list/activ/",
  // routes des popups
  URL_POPUPS_ACTIV: URL_API + "api/popups/list/activ/",
  // routes contact
  URL_EMAIL_CONTACT_UPLOAD: URL_API + "api/emails/contact-upload",
  URL_FETCH_DATA_CONFIG_SITE: URL_API + "api/config/config-from-name/",
  // routes client
  URL_FETCH_CLIENT: URL_API + "api/clients/show/",
  // routes panier
  URL_STORE_TRANSACTION : URL_API + "api/paiements/create"
};
