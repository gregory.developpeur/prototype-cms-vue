/* eslint-disable semi */
/* eslint-disable quotes */
/* eslint-disable space-before-function-paren */
import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/scss/styles.scss";
import "es6-promise/auto"; // pour les anciens navigateur tel que IE
import VueCookie from "vue-cookie";

import Vuetify from "vuetify";
import wysiwyg from "vue-wysiwyg";
import VueJwtDecode from "vue-jwt-decode";
import axios from "axios";
Vue.prototype.$axios = axios;
Vue.config.productionTip = false;

// this.$axios.defaults.headers.common["Authorization"] = "Bearer " + tokenData.token;
// this.$axios.defaults.headers.post["Content-Type"] = "application/json";

// var CryptoJS = require("crypto-js");
// var hash = CryptoJS.SHA512("Message");
// hash = hash.toString(CryptoJS.enc.Base64);
// var decodedHash = CryptoJS.enc.Base64.parse(hash).toString();
// console.log("encode", hash);
// console.log("decode", decodedHash);

axios.defaults.headers.common["siteHost"] = process.env.VUE_APP_DB_HOST;
axios.defaults.headers.common["siteDatabase"] = process.env.VUE_APP_DB_DATABASE;
axios.defaults.headers.common["siteUsername"] = process.env.VUE_APP_DB_USER;
axios.defaults.headers.common["sitePassword"] = process.env.VUE_APP_DB_PASSWORD;
axios.defaults.headers.common["siteName"] = process.env.VUE_APP_SITE_ALIAS;
axios.defaults.headers.common["siteKey"] = process.env.VUE_APP_SITE_KEY;

Vue.use(VueJwtDecode);
Vue.use(wysiwyg, {});
Vue.use(Vuetify);

Vue.use(VueCookie);
// Vue.use(axios);
Vue.use(Vuex);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
