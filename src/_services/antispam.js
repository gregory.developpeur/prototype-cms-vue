export function antiSpam(ipUser) {
  let varIpInternaute = "";
  let varIntervalTemps = null;
  let varDifferenceTime = null;
  let limitespam = 3;

  if (!localStorage.getItem("varIpInternaute")) {
    localStorage.setItem("varIpInternaute", ipUser);
    localStorage.setItem("varIntervalTemps", Date.now());
    return "first send"
    // console.log("je poste un premier message");
  } else {
    varIpInternaute = ipUser;
    varIntervalTemps = Date.now();

    // on compare les 2 ips
    if (localStorage.getItem("varIpInternaute") == varIpInternaute) {
      varDifferenceTime = Math.abs(Math.floor((localStorage.getItem("varIntervalTemps") - varIntervalTemps) / 1000 / 60));
    }

    if (varDifferenceTime >= limitespam) {
      // console.log("je peux poster");
      localStorage.removeItem("varIpInternaute");
      localStorage.removeItem("varIntervalTemps");
      return "can resend"
    } else {
      return "spam protect"
    }
  }
}
