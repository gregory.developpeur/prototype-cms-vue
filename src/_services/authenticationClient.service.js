/* eslint-disable handle-callback-err */
/* eslint-disable no-unused-vars */
/* eslint-disable quotes */
/* eslint-disable space-before-function-paren */
/* eslint-disable semi */
import axios from "axios";
// import { mapState } from "vuex";
import { getToken, getRoles, getPermission, hasRole, decodeToken } from "@brickblock/authorisation-library";
import { getHeaderClient } from "@/config-client";
// ne peut
import store from "../../src/store";
import router from "../../src/router";

import VueJwtDecode from "vue-jwt-decode";

const URL_API = process.env.VUE_APP_URL_DISTANT_API;
const SITE_ALIAS = process.env.VUE_APP_SITE_ALIAS;
const URL_TEST_OPEN = URL_API + "api/open"; // unprotected route
const URL_CLIENT = URL_API + "api/client"; // protected route
// const URL_USER = URL_API + "api/user"; // protected route
// const URL_LOGIN = URL_API + "api/login"; // get token user
// const URL_LOGIN_CLIENT= URL_API + "api/client/login"; // get token user
const URL_LOGIN_CLIENT= URL_API + "api/auth/clients/login"; // get token user
// const URL_LOGIN = URL_API + "api/auth/login"; // get token user
const URL_TEST_CLOSED = URL_API + "api/closed"; // protected route user

// var token = null;
var authUserClient = {};
var currentUserClient = {};

// ...mapState({
//   userStore: state => state.userStore
// })

function logout() {
  console.log("je dois logout")
  store.dispatch("clearAuthUserClient");
  window.localStorage.removeItem("authUserClient");
  // router.push("/");
}

function login(credentials, pageReturn) {
  console.log("pageReturn", pageReturn);
  let payLoadLogin = credentials;
  axios
    .post(URL_LOGIN_CLIENT, payLoadLogin)
    .then(response => {
      authUserClient = response.data;
      console.log("etape1", authUserClient)
      window.localStorage.setItem("authUserClient", JSON.stringify(authUserClient));
      axios
        .get(URL_CLIENT, { headers: getHeaderClient() })
        .then(response => {
          if (response.data.user.actif) {
            console.log("etape2", response.data.user)
            window.localStorage.setItem("infoClient", response.data.user.data);
            window.localStorage.setItem("clientId", response.data.user.id);
            store.dispatch("initClient", { infoClient: response.data.user, authUserClient: authUserClient });
            if (pageReturn !=='') {
              router.push({ name: pageReturn, query: { action: "logged" } });
            } else {
              router.push({ name: "compte-client", query: { action: "logged" } });
            }

          } else {
            logout();
          }
        })
        .catch(error => {
          console.log("error");
        });
    })
    .catch(error => {
      console.log("error = ", error);
    });
}

function checkRoles() {
  // if (window.localStorage.getItem("authUserClient")) {
  //   let userClientObj = JSON.parse(window.localStorage.getItem("authUserClient"));
  //   // console.log("userClientObj", userClientObj);
  //   axios
  //     .get(URL_USER, { headers: getHeader() })
  //     .then(response => {
  //       // store.dispatch("setUserRole", response.data.user.roles);
  //     })
  //     .catch(error => {
  //       console.log("error");
  //       logout();
  //     });
  // }
}

export const authenticationClientService = {
  logout,
  login,
  checkRoles
  // currentUser: currentUserSubject.asObservable(),
  // get currentUserValue () {
  //   return currentUserSubject.value;
  // }
};
