/* eslint-disable no-unreachable */
/* eslint-disable padded-blocks */
/* eslint-disable no-trailing-spaces */
/* eslint-disable no-global-assign */
/* eslint-disable quotes */
/* eslint-disable semi */
/* eslint-disable space-before-function-paren */
import Vue from "vue";
// import jsonConfig from "@/config-data/config-products.json";
import jsonConfigBuilder from "@/config-data/config-products.json";
// import jsonConfigCategories from "@/config-data/categories.json";
import slugme from "slugme";
import SlVueTreeComponent from "../Categories/SlVueTreeComponent";
import adminImages from "../Medias/AdminImages";
import adminPdf from "../Medias/AdminPdf";
// import axios from "axios";

const URL_API = process.env.VUE_APP_URL_DISTANT_API;
const URL_CATEGORIES_LIST_FROM = URL_API + "api/categories-from-name/";
// const URL_FETCH_DATA = URL_API + "api/popups/show/";
var URL_FETCH_DATA;
// const URL_IMAGES_FETCH = process.env.VUE_APP_URL_DISTANT_API + "api/v1/images";

export default {
  created: function() {
    this.fetchCombi();
    this.loadTableImages();
    this.loadTablePdfs();
    this.loadTableVideos();
    // if (this.editMode) {
    //   this.libelleAction = "Modifier";
    //   this.fetchDataFiche();
    //   // this.fetchImages();
    // }
    // this.moduleName = this.$route.params.module;
    this.jsonConfigBuilder.forEach(mod => {
      if (mod.module === this.moduleName) {
        this.tableName = mod.tableName;
        this.moduleJson = mod;
      }
    });
    this.fetchCategories();
  },
  components: {
    "admin-images": adminImages,
    "admin-pdf": adminPdf,
    "sl-vue-tree-component": SlVueTreeComponent
  },
  props: [
    "totalTitres",
    "totalDescriptions",
    "totalSwitchs",
    "totalOuis",
    "totalListes",
    "actionSubmit",
    "switchLabels",
    "listeLabels",
    "contentListes",
    "titreLabels",
    "ouiLabels",
    "ouiValues",
    "descriptionLabels",
    "declinaisonsLabels",
    "totalPdfs",
    "totalImages",
    "totalVideos",
    "News",
    "listeLibelleImages",
    "listeLibellePdfs",
    "listeLibelleVideos",
    "moduleName",
    "action"
  ],
  mounted: function() {
    this.loadUrls();
    this.fetchDataCategories();
    // this.setHeadersPrixSimple();
    // this.setHeadersDeclinaisons();
    // if (this.action == "edit") {
    //   this.libelleAction = "Modifier";
    //   this.fetchDataFiche();
    //   // this.fetchDataDeclinaisons();
    // }
    // this.fixDefaultValuePrices();
    // this.getTnOptions();
  },
  data() {
    return {
      tableName: null,
      libelleAction: "Créer",
      jsonConfigBuilder,
      // jsonConfig,
      moduleJson: {},
      // jsonConfigCategories,
      modulesCategories: [],
      msgError: "",
      panel: [true], // pour expand champs descriptions ex: [true, true] ouvrira les 2 premiers expands et pas le reste
      area: "fiche", // variable tabs
      valid: false,
      recupData: null,
      recupDataPrix: null,
      recupRawData: null,
      isCodePromo: -1,

      // Variables models forms
      mTitre: [],
      mOui: [],
      mSwitch: [],
      mListe: [],
      mDescription: [],
      mImages: [],
      mPdfs: [],
      mVideos: [],

      // traitement spé sur champ Oui
      modeleOui: "",

      // variables déclinaisons
      selected: [], // voir a virer cette variable ?
      selectedCombi: "",
      isEditDeclinaison: false,
      pagination: {
        sortBy: "reference"
      },
      selectedDecli: {
        reference: "",
        prices: [],
        poids: 0,
        quantite: 0,
        properties: []
      },
      selectedDecliIndex: null,
      headers: [],
      declinaisons: [],

      // Variables champs prix // a utiliser en module ensuite
      poidsProduit:0,
      listQte: [],
      typeQte: "",
      prixKg: 0,
      qte:0,
      labelsPrice: [],
      labelsPriceDeclinaisons: [],
      newSimplePrice: [],
      newDecliPrice: [],
      lotDecliPrice: [],
      numberRules: [v => /^[+-]?\d+(\.\d+)?$/.test(v) || "Ce n'est pas un montant valide"],
      orderRules: [v => /^[+-]?\d+(\.\d+)?$/.test(v) || "ce n'est pas un ordre valide"],
      listeCombi: [],
      typePrix: "simple",
      itemsPrice: [{ text: "Prix simple", value: "simple" }, { text: "Utiliser les déclinaisons", value: "declinaisons" }],
      dialog: false,
      labelProperties: [],

      // Variables images
      tabPrevImgTn: [],
      tabImgName: [],
      tabImg: [],
      notifications: false,
      sound: true,
      widgets: false,
      listeImages: {},
      tableImages: [],
      imgSrc: "",
      selectedFile: null,
      selectedCropFile: null,
      optionsTn: {},

      // variables pdf
      tablePdfs: [],
      tabPdfName: [],
      tabPrevPdfTn: [],

      // variables videos
      tableVideos: [],
      tabVideoName: [],
      tabPrevVideoTn: [],

      // Variables categories
      enteteCategories: "",
      afficheCat: {
        cat: false,
        scat1: false,
        scat2: false,
        scat3: false,
        scat4: false
      },
      nodes: [],
      nodesCat: [],
      nodesScat1: [],
      nodesScat2: [],
      nodesScat3: [],
      selectedCat: null,
      selectedScat1: null,
      selectedScat2: null,
      selectedScat3: null,
      iCat: null,
      iScat1: null,
      iScat2: null,
      iScat3: null,

      // Snackbar
      snackbar: {
        actif: false,
        multiline: true,
        text: ""
      }
    };
  },
  methods: {
    // ////////////////////////////////////////////////////////////////////
    // /////////////////////// METHODES GENERALES /////////////////////////
    // ////////////////////////////////////////////////////////////////////
    loadUrls() {
      URL_FETCH_DATA = URL_API + "api/" + this.tableName + "/show/";
    },

    secondLevelCall() {
      // fonction appelé d'un parent
    },
    backToList() {
      //this.$router.push({ name: "admin-news-liste" });
    },

    // Récupérations des données de la bdd si mode edit
    fetchDataFiche() {
      let id = this.$route.params.id;
      this.$axios
        .get(URL_FETCH_DATA + id)
        .then(response => {
          // attention recupData ne contient pas toutes les données. Uniquement celles des champs dynamiques
          this.recupRawData = response.data.data;
          this.News.actif = response.data.data.actif;
          this.News.ordre = response.data.data.ordre;
          this.recupData = JSON.parse(response.data.data.data);
          console.log("this.recupData ", this.recupData);
          // todo moulinette pour faire correspondre les datas enregistrés avec les imput du form
          // récupération des titres
          for (var i = 1; i < this.totalTitres + 1; i++) {
            this.mTitre[i] = this.recupData["titre" + i];
          }
          // récupération des descriptions
          for (var n = 1; n < this.totalDescriptions + 1; n++) {
            this.mDescription[n] = this.recupData["description" + n];
          }

          // récupération des switchs
          for (var k = 1; k < this.totalSwitchs + 1; k++) {
            this.mSwitch[k] = this.recupData["switch" + k];
          }

          // récupération des listes
          for (var l = 1; l < this.totalListes + 1; l++) {
            this.mListe[l] = this.recupData["liste" + l];
          }

          // récupération des champs oui
          for (var j = 0; j < this.totalOuis + 1; j++) {
            this.mOui[j] = this.recupData["oui" + j];
            if (this.mOui[j] === this.ouiValues[j - 1]) {
              this.modeleOui = this.mOui[j];
            }
          }

          // récupération des images
          for (let o = 0; o < this.tableImages.length; o++) {
            this.mImages[o] = this.recupData["image" + parseInt(o + 1)];
          }

          // récupération des pdf
          for (let o = 0; o < this.tablePdfs.length; o++) {
            this.mPdfs[o] = this.recupData["pdf" + parseInt(o + 1)];
          }

          // récupération des videos
          for (let o = 0; o < this.tableVideos.length; o++) {
            this.mVideos[o] = this.recupData["video" + parseInt(o + 1)];
          }

          // récupération des catégories + traitement pour afficher les select
          console.log("recupData.categorie", this.recupData.categorie);
          this.changeCat(this.recupData.categorie, 0, true);

          if (this.recupData.scat1 !== "" && this.recupData.scat1 !== null) {
            // console.log("recupData.scat1", this.recupData.scat1);
            this.changeCat(this.recupData.scat1, 1, true);
          }

          if (this.recupData.scat2 !== "" && this.recupData.scat2 !== null) {
            this.changeCat(this.recupData.scat2, 2, true);
          }

          this.selectedCat = this.recupData.categorie;
          this.selectedScat1 = this.recupData.scat1;
          this.selectedScat2 = this.recupData.scat2;
          this.selectedScat3 = this.recupData.scat3;

          // récup info quantité fiche
          this.typeQte = this.recupData.typeQte
          this.qte = this.recupData.qte

          // console.log("selectedCat", this.selectedCat);
          // console.log("selectedScat1", this.selectedScat1);
          // console.log("selectedScat2", this.selectedScat2);

          // récupération dataprix
          this.fetchDataDeclinaisons(response.data.data);
        })
        .then({})
        .catch(error => {
          console.log("error = ", error);
        });
    },
    // affiche la bonne zone en fonction du choix tabs
    changeArea(newZone) {
      this.area = newZone;
    },
    // retour valeurs pour le component parent
    passingDataAndSubmit() {
      let allData = {};
      let allPrices = {};

      // todo récupérer d'une config et nom des champs renseignés
      for (var i = 1; i < this.totalTitres + 1; i++) {
        if (this.mTitre[i] !== undefined && this.mTitre[i] !== null) {
          allData["titre" + i] = this.mTitre[i];
        } else {
          allData["titre" + i] = "";
        }
      }

      // intégration champs oui
      if (this.action == "edit") {
        for (var j = 0; j < this.totalOuis; j++) {
          if (this.modeleOui === this.ouiValues[j]) {
            allData["oui" + (j + 1)] = this.modeleOui;
          } else {
            allData["oui" + (j + 1)] = "";
          }
        }
      } else {
        // ok mode normal create
        for (var m = 0; m < this.totalOuis; m++) {
          if (this.mOui[m] !== undefined && this.mOui[m] !== null) {
            if (this.mOui === this.ouiValues[m]) {
              allData["oui" + (m + 1)] = this.mOui;
            } else {
              allData["oui" + (m + 1)] = "";
            }
          } else {
            allData["oui" + (m + 1)] = "";
          }
        }
      }

      // intégration des champs switch
      for (var k = 1; k < this.totalSwitchs + 1; k++) {
        if (this.mSwitch[k] !== undefined && this.mSwitch[k] !== null) {
          allData["switch" + k] = this.mSwitch[k];
        } else {
          allData["switch" + k] = "";
        }
      }

      // intégration des champs listes
      for (var l = 1; l < this.totalListes + 1; l++) {
        if (this.mListe[l] !== undefined && this.mListe[l] !== null) {
          allData["liste" + l] = this.mListe[l];
        } else {
          allData["liste" + l] = "";
        }
      }

      // intégration des champs description
      for (var n = 1; n < this.totalDescriptions + 1; n++) {
        if (this.mDescription[n] !== undefined && this.mDescription[n] !== null) {
          allData["description" + n] = this.mDescription[n];
        } else {
          allData["description" + n] = "";
        }
      }

      // intégration des images
      for (let o = 0; o < this.tableImages.length; o++) {
        allData["image" + parseInt(o + 1)] = this.mImages[o];
      }
      // intégration des pdf
      for (let o = 0; o < this.tablePdfs.length; o++) {
        allData["pdf" + parseInt(o + 1)] = this.mPdfs[o];
      }
      // intégration des videos
      for (let o = 0; o < this.tableVideos.length; o++) {
        allData["video" + parseInt(o + 1)] = this.mVideos[o];
      }

      // intégration des champs prix
      // variables config prix
      allPrices.selectedCombi = this.selectedCombi;
      allPrices.typePrix = this.typePrix;
      allPrices.prixKg = this.prixKg
      allPrices.poidsProduit = this.poidsProduit

      // intégration prix simples
      allPrices.simplePrice = [];
      for (var p = 0; p < this.labelsPrice.length; p++) {
        if (this.newSimplePrice[p] !== undefined && this.newSimplePrice[p] !== null) {
          let element = {};
          element["prix" + (p + 1)] = this.newSimplePrice[p];
          allPrices.simplePrice.push(element);
        } else {
          allPrices.simplePrice["prix" + (p + 1)] = 0;
        }
      }

      // intégration prix déclinaisons
      allPrices.declinaisons = [];
      if (this.declinaisons !== undefined) {
        if (this.declinaisons.length > 0) {
          this.declinaisons.forEach((element, index) => {
            allPrices.declinaisons.push(element);
          });
        }
      }

      console.log("allPrices >>", allPrices);

      // intégration des catégories
      // console.log("this.selectedCat", this.selectedCat);
      allData["categorie"] = this.selectedCat;
      allData["scat1"] = this.selectedScat1;
      allData["scat2"] = this.selectedScat2;
      allData["scat3"] = this.selectedScat3;
      allData["typeQte"] = this.typeQte;
      allData["qte"] = this.qte;
      console.log("allData", allData);
      // vérification catégories (et autres) avant submit
      this.checkValidationForm(allData, allPrices, this.News.actif, this.News.ordre);
    },
    checkValidationForm(dataForm, pricesForm, actifFiche, ordreFiche) {
      // vérif catégorie
      if (dataForm["categorie"] == null || dataForm["categorie"] === "") {
        this.msgError = "Vous devez spécifier au moins une catégorie";
        this.snackbar.text = this.msgError;
        this.snackbar.actif = true;
        return false;
      }
      // vérif ordre combinaisons déclinaisons
      if (pricesForm.typePrix === "declinaisons" && (pricesForm.selectedCombi == null || pricesForm.selectedCombi === "")) {
        this.msgError = "Vous devez spécifier dans quel ordre les déclinaisons doivent être choisis";
        this.snackbar.text = this.msgError;
        this.snackbar.actif = true;
        return false;
      }

      // a prévoir éventuellement
      // vérif qu' il n' y a pas de déclinaisons vides en fonction du typecombi choisi
      // check typePrix == déclinaions
      // split sur property
      // boucle sur property
      // boucle sur déclinaisons

      this.$emit("returnValuesForm", { globalFields: dataForm, priceFields: pricesForm, activeField: actifFiche, ordreField: ordreFiche });
      // this.$emit("returnValuesForm", dataForm);
    },
    checkFormat(sequence, typeVerif) {
      switch (typeVerif) {
        case "float":
          console.log("verif type a faire");
          break;
        case "integer":
          console.log("verif type a faire");
          break;
      }
    },
    deleteFiche() {
      this.$emit("returnDelete");
    },

    checkIfCodePromo() {
      this.isCodePromo = this.moduleName.indexOf("codepromo")
    },

    // //////////////////////////////////////////////////////////////////////////
    // /////////////////////// METHODES LIES AUX VIDEOS /////////////////////////
    // //////////////////////////////////////////////////////////////////////////
    loadTableVideos() {
      for (var i = 0; i < this.totalVideos; i++) {
        this.tableVideos.push([]);
      }
    },

    // //////////////////////////////////////////////////////////////////////////
    // /////////////////////// METHODES LIES AUX PDF ////////////////////////////
    // //////////////////////////////////////////////////////////////////////////
    onChildClickPdf($event) {
      console.log("event", $event);
      if (this.action == "edit") {
        let tempoData = JSON.parse(this.recupRawData.data);
        $event.forEach((element, index) => {
          Vue.set(this.tabPdfName, index, element);
          tempoData["pdf" + (index + 1)] = element;
        });
        tempoData = JSON.stringify(tempoData);
        this.recupRawData.data = tempoData;
      }
      this.mPdfs = $event;
    },
    loadTablePdfs() {
      for (var i = 0; i < this.totalPdfs; i++) {
        this.tablePdfs.push([]);
      }
    },
    // //////////////////////////////////////////////////////////////////////////
    // /////////////////////// METHODES LIES AUX IMAGES /////////////////////////
    // //////////////////////////////////////////////////////////////////////////
    getTnOptions() {
      if (this.moduleJson) {
        this.optionsTn = this.moduleJson.tabTn;
        // this.optionsTn.redim = this.moduleJson.redim;
        // this.optionsTn.bgTn = this.moduleJson.bgTn;
      }
    },
    onChildClick($event) {
      if (this.action == "edit") {
        let tempoData = JSON.parse(this.recupRawData.data);
        $event.forEach((element, index) => {
          Vue.set(this.tabImgName, index, element);
          tempoData["image" + (index + 1)] = element;
        });
        tempoData = JSON.stringify(tempoData);
        this.recupRawData.data = tempoData;
      }
      this.mImages = $event;
    },
    loadTableImages() {
      for (var i = 0; i < this.totalImages; i++) {
        this.tableImages.push([]);
      }
    },
    // a quoi sert cette fonction T_T
    infosImgPrev(eventImg) {
      this.tabPrevImgTn = eventImg;
    },
    infosImg(eventImg) {
      this.tabImg = eventImg;
    },
    infosImgName(eventImg) {
      this.tabImgName = eventImg;
    },
    setImage(e) {
      this.selectedFile = e.target.files[0];
      this.imageName = this.setImageName(this.selectedFile.name);
      this.uploadPercentage = 0;
      if (!this.selectedFile.type.includes("image/")) {
        alert("Please select an image file");
        return;
      }
      if (typeof FileReader === "function") {
        const reader = new FileReader();
        reader.onload = event => {
          this.imgSrc = event.target.result;
          // rebuild cropperjs with the updated source
          this.$refs.cropper.replace(event.target.result);
        };
        reader.readAsDataURL(this.selectedFile);
      } else {
        alert("Sorry, FileReader API not supported");
      }
    },
    setImageName(rawName) {
      let newName = rawName;
      let tab1 = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
      let tab2 = "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn";
      let rep2 = tab1.split("");
      let rep = tab2.split("");
      // let myarray = new Array()
      let myarray = [];
      let i = -1;
      while (rep2[++i]) {
        myarray[rep2[i]] = rep[i];
      }
      myarray["Œ"] = "OE";
      myarray["œ"] = "oe";

      newName = newName.replace(/./g, function($0) {
        return myarray[$0] ? myarray[$0] : $0;
      });
      newName = newName.replace(/[^.a-zA-Z0-9_-]/g, "");
      var random = Math.floor(Math.random() * 10000 + 1000);
      var unix = Math.round(+new Date() / 1000);
      return unix + random + newName;
    },

    // //////////////////////////////////////////////////////////////////////////////
    // /////////////////////// METHODES LIES AUX CATEGORIES /////////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    // rafraichir la liste des catégories si modifications structure catégories
    refreshCategories($event) {
      this.cancelCat(3);
      this.cancelCat(2);
      this.cancelCat(1);
      this.cancelCat(0);
      this.fetchDataCategories("refresh");
      this.$emit("refreshCat", true);
    },
    // récupérer la liste des catégories pour le select
    fetchCategories() {
      this.enteteCategories = [];
      this.moduleJson.categories.forEach((el, idx) => {
        if (el.affiche === true) {
          this.enteteCategories.push(el.entete);
        }
      });
      this.modulesCategories.slug = this.moduleJson.confCatSlug;

      // traitement check if codepromo
      this.checkIfCodePromo()
    },
    // annuler la sélection de la catégorie en cours
    cancelCat(lvl) {
      switch (lvl) {
        case 0:
          this.selectedCat = null;
          this.selectedScat1 = null;
          this.selectedScat2 = null;
          this.selectedScat3 = null;
          this.iCat = null;
          this.iScat1 = null;
          this.iScat2 = null;
          this.iScat3 = null;

          break;
        case 1:
          this.selectedScat1 = null;
          this.selectedScat2 = null;
          this.selectedScat3 = null;

          this.iScat1 = null;
          this.iScat2 = null;
          this.iScat3 = null;
          break;
        case 2:
          this.selectedScat2 = null;
          this.selectedScat3 = null;
          this.iScat2 = null;
          this.iScat3 = null;
          break;
        case 3:
          this.selectedScat3 = null;
          this.iScat3 = null;
          break;
      }
    },
    // réinitialise les catégories filles lors qu'on change de catégorie parent
    resetCat(lvl) {
      console.log("je reset", lvl);
      /// bug ?
      switch (lvl) {
        case 0:
          this.iCat = null;
          this.iScat1 = null;
          this.iScat2 = null;
          this.iScat3 = null;
          this.selectedScat1 = null;
          this.selectedScat2 = null;
          this.selectedScat3 = null;
          break;
        case 1:
          this.iScat1 = null;
          this.iScat2 = null;
          this.iScat3 = null;
          this.selectedScat2 = null;
          this.selectedScat3 = null;
          break;
        case 2:
          this.iScat2 = null;
          this.iScat3 = null;
          this.selectedScat3 = null;
          break;
      }
      console.log("fin exec reset");
    },
    // changement de catégories
    changeCat(event, level, initial) {
      if (initial == false) {
        this.resetCat(level); // a pas faire au premier chargement
      }

      let noeud;
      switch (level) {
        case 0:
          noeud = this.nodes;
          break;
        case 1:
          noeud = this.nodes[this.iCat].children;
          break;
        case 2:
          noeud = this.nodes[this.iCat].children[this.iScat1].children;
          break;
      }
      for (var i = 0; i < noeud.length; i++) {
        if (noeud[i].slug === event) {
          switch (level) {
            case 0:
              this.iCat = i;
              this.nodesScat1 = noeud[i].children;
              break;
            case 1:
              this.iScat1 = i;
              this.nodesScat2 = noeud[i].children;
              break;
            case 2:
              this.iScat2 = i;
              this.nodesScat3 = noeud[i].children;
              break;
          }
        }
      }
    },
    // récupération des datas des catégories
    fetchDataCategories(typeCat) {
      if (typeCat === "refresh") {
        this.nodes = [];
      }
      // récupération de la liste des catégories
      this.$axios
        .get(URL_CATEGORIES_LIST_FROM + this.modulesCategories.slug)
        .then(response => {
          // console.log("retour cat", response.data.data);
          // construction de la liste des données du module (news)
          response.data.data.forEach(el => {
            let tempnode = JSON.parse(el.data);
            this.nodes.push(tempnode.categories);
          });
          // this.nodes = JSON.parse(this.nodes);
          // this.nodesCat = this.nodes;

          // if (this.nodesCat.length == 1) {
          //   this.selectedCat = this.nodesCat[0].slug;
          // }
        })
        .then(resp => {
          this.nodes = JSON.parse(this.nodes);
          this.nodesCat = this.nodes;

          if (this.nodesCat.length == 1) {
            this.selectedCat = this.nodesCat[0].slug;
          }

          this.setHeadersPrixSimple();
          this.setHeadersDeclinaisons();
          this.setTypeQte();
          if (this.action == "edit") {
            this.libelleAction = "Modifier";
            this.fetchDataFiche();
            // this.fetchDataDeclinaisons();
          }
          this.fixDefaultValuePrices();
          this.getTnOptions();
        })
        .catch(error => {
          console.log("error = ", error);
        });
    },

    // ////////////////////////////////////////////////////////////////////////////////////////
    // /////////////////////// METHODES LIES AUX PRIX ET DECLINAISONS /////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////
    setTypeQte() {
      this.listQte.push({ libelle: "Aucun", value:""})
      if (this.moduleJson.isUnite == true) {
        this.listQte.push({ libelle: "A l'unité", value:"unite"} )
      }
      if (this.moduleJson.isPoids == true) {
        this.listQte.push({ libelle: "Au poids", value:"poids"} )
      }
      if (this.moduleJson.isInfinite == true) {
        this.listQte.push({ libelle: "Sans limite", value:"infinite"} )
      }
    },
    addDeclinaison() {
      if (this.selectedDecli.reference !== "") {
        // push selectedDecli to declinaisons
        let id = this.declinaisons.length + 1;
        let newObj = {
          id: id,
          poids: this.selectedDecli.poids,
          prices: this.selectedDecli.prices,
          properties: this.selectedDecli.properties,
          quantite: this.selectedDecli.quantite,
          reference: this.selectedDecli.reference
        };

        this.declinaisons.push(newObj);
        this.cancelEditDecli();
      }
    },
    validEditDeclinaison() {
      let id = this.selectedDecliIndex;
      this.declinaisons[id] = this.selectedDecli;
      Vue.set(this.declinaisons, id, this.selectedDecli);

      console.log("je dois modifier la ref dans le tableau");
      console.log("declinaisons", this.declinaisons);
      this.cancelEditDecli();
    },
    cancelEditDecli() {
      this.selectedDecli = {
        reference: "",
        prices: [],
        poids: 0,
        quantite: 0,
        properties: []
      };
      this.isEditDeclinaison = false;
      this.selectedDecliIndex = null;
    },
    editDeclinaison(ref, idx) {
      // Note de développement
      // obligé de passer par JSON.stringify et JSON.parse car l'objet et tableaux
      // sont passés par référence ce qui modifait également le tableau initial
      // comportement qu'on ne voulait pas cela allait poser problème par la suite
      let refStringify = JSON.stringify(ref);
      this.selectedDecli = JSON.parse(refStringify);
      this.selectedDecliIndex = idx;
      this.isEditDeclinaison = true;
      window.location.href = "#detaildecli";
    },
    deleteDeclinaison(id, index) {
      this.declinaisons.splice(index, 1);
    },
    // tri table déclinaisons
    changeSort(column) {
      if (this.pagination.sortBy === column) {
        this.pagination.descending = !this.pagination.descending;
      } else {
        this.pagination.sortBy = column;
        this.pagination.descending = false;
      }
    },
    // récupération des libellés prix
    setHeadersPrixSimple() {
      this.moduleJson.prix.forEach(prix => {
        this.labelsPrice.push({ text: prix.libelle, value: prix.value });
      });
    },
    // récupération des headers du tableau des déclainaisons
    setHeadersDeclinaisons() {
      // let optionsDecli = jsonConfig.news.declinaisons;
      let optionsDecli = [];
      optionsDecli.push({ text: "Référence", value: "reference" });

      // définition des headers des options
      this.moduleJson.declinaisons.forEach(decli => {
        optionsDecli.push({ text: decli, value: slugme(decli) });
      });
      // définition des headers prix
      this.moduleJson.prixDeclinaisons.forEach((prix, index) => {
        // optionsDecli.push({ text: decli, value: "prix" + (index + 1) });
        this.labelsPriceDeclinaisons.push({
          text: prix.libelle,
          value: prix.type
        });
        optionsDecli.push({ text: prix.libelle, value: prix.type });
      });
      // définition du header quantuté
      optionsDecli.push({ text: "Quantité", value: "quantite" });
      // définition du header poids
      let champoids = this.moduleJson.poids[0];
      optionsDecli.push({ text: champoids, value: "poids" });
      this.headers = optionsDecli;
    },
    // récupération des datas enregistrés des déclinaisons
    fetchDataDeclinaisons(response) {
      let dataPrix = JSON.parse(response.dataprix);
      this.typePrix = dataPrix.typePrix;

      // prix normaux
      dataPrix.simplePrice.forEach((el, index) => {
        let champs = "prix" + parseInt(index + 1);
        this.newSimplePrice[index] = el[champs];
      });

      // prix décli
      this.declinaisons = dataPrix.declinaisons;
      this.selectedCombi = dataPrix.selectedCombi;
      this.prixKg = dataPrix.prixKg
      this.poidsProduit = dataPrix.poidsProduit
      // let recupDataDeclinaisons = []
    },
    // modification des prix des déclinaisons d'une seule traite
    modifPrixLot() {
      self = this;
      let idx = 0;
      // eslint-disable-next-line no-unused-vars
      for (let [refkey, refvalue] of Object.entries(this.declinaisons)) {
        for (let [key, value] of Object.entries(refvalue)) {
          if (key === "prices") {
            value.forEach(function(item, index) {
              if (self.lotDecliPrice[index] !== "" && self.lotDecliPrice[index] !== undefined) {
                item = self.lotDecliPrice[index];
                self.declinaisons[idx].prices[index] = item;
                Vue.set(self.declinaisons, idx, self.declinaisons[idx]);
              }
            });
          }
        }
        idx++;
      }
      // réinitialisation des champs de lots
      self.lotDecliPrice.forEach(function(item, index) {
        self.lotDecliPrice[index] = 0;
      });
    },
    // calcul des différentes combinaisons possible des déclinaisons
    combinaisons(tab, taille) {
      // Cas de base : pour une taille de 1, on retourne simplement le tableau
      if (taille === 1) {
        return tab;
      } else {
        // Cas général :
        // Tableau de résultats final
        var result = [];
        // Calcul des combinaisons pour la taille inférieure (appel récursif)
        var combinaisons = this.combinaisons(tab, taille - 1);
        // Création des nouvelles combinaisons à partir de celle de taille inférieure:
        // On ajoute chaque caractère de départ devant chacune des combinaisons trouvées

        tab.forEach(function(caractere) {
          combinaisons.forEach(function(combinaison) {
            let concatOk = true;
            let checkCombi = combinaison.split(" >>> ");

            checkCombi.forEach(function(c) {
              checkCombi.forEach(function(c2) {
                if (c2 === caractere) {
                  concatOk = false;
                }
              });
            });

            if (concatOk) {
              result.push(caractere + " >>> " + combinaison);
            }
          });
        });

        // Le résultat est aussi bien les combinaisons de taille inférieure que celle générées
        // On met donc les deux ensemble
        result = result.concat(combinaisons);
        // on supprime les doublons
        result = this.uniq(result);
        return result;
      }
    },
    // Suppression doublons tableau / merci stackoverflow pour la fonction
    uniq(a) {
      return a.sort().filter(function(item, pos, ary) {
        return !pos || item !== ary[pos - 1];
      });
    },
    // on charge les labels de la liste des déclinaisons
    fetchCombi() {
      let properties = [];
      this.declinaisonsLabels.forEach(function(el) {
        properties.push(el);
      });
      this.labelProperties = properties;
      if (this.labelProperties.length > 0) {
        this.listeCombi = this.combinaisons(properties, properties.length);
      }
    },
    fixDefaultValuePrices() {
      this.labelsPriceDeclinaisons.forEach((el, idx) => {
        this.selectedDecli.prices.push(0);
      });

      this.labelsPrice.forEach((el, idx) => {
        this.newSimplePrice.push(0);
      });
    }
  }
};
