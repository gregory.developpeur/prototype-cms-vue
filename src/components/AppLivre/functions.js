import { antiSpam } from "../../_services/antispam";

const URL_API = process.env.VUE_APP_URL_DISTANT_API;
const URL_LIVREDOR_ACTIV = URL_API + "api/livres/list/activ/";
const URL_LIVREDOR_CREATE = URL_API + "api/livres/create";
const URL_EMAIL_LIVRE_RGPD = URL_API + "api/emails/livre-rgpd";
const URL_EMAIL_LIVRE_ADMIN = URL_API + "api/emails/livre-admin";
const URL_FETCH_DATA_CONFIG_SITE = URL_API + "api/config/config-from-name/";

const MODULENAME = "livre1";
const CAT_MODULE = "livre-dor";
// import { sendMailMixin } from "../../mixins/sendMailMixin.js"; // utilisation mixin en attente

export default {
  // mixins: [sendMailMixin],
  created: function() {
    this.fetchData(MODULENAME);
    this.fetchDataConfig("coords");
    // this.hello();
  },
  mounted() {
    this.checkIfvalidate();
  },
  data() {
    return {
      messageClient: "",
      formSubmitedIsOK: false,
      showForm: false,
      showRgpdOK: false,
      listeData: [],
      moduleName: MODULENAME,
      emailAdmin: null,
      errorForm: false,
      globalError: "",
      formLivre: [
        {
          id: "champ1",
          libelle: "Pseudo",
          value: "",
          type: "text",
          required: true,
          placeHolder: "",
          error: "",
          groupClass: "form-group",
          inputClass: "form-control",
          labelClass: ""
        },
        {
          id: "champ2",
          libelle: "Email",
          value: "",
          type: "email",
          required: true,
          placeHolder: "name@example.com",
          error: "",
          groupClass: "form-group",
          inputClass: "form-control",
          labelClass: ""
        },
        {
          id: "champ3",
          libelle: "Message",
          value: "",
          type: "textarea",
          required: true,
          placeHolder: "",
          error: "",
          groupClass: "form-group",
          inputClass: "form-control",
          labelClass: ""
        },
        {
          id: "champ4",
          libelle: "Email visible ?",
          value: "",
          type: "checkbox",
          required: false,
          placeHolder: "",
          error: "",
          groupClass: "form-check",
          inputClass: "form-check-input",
          labelClass: "form-check-label"
        }
      ]
    };
  },
  methods: {
    saveFromDecodedQuery(decodedQuery) {
      let newMsg = {};
      const arrayDecodedQuery = decodedQuery.split("||");
      arrayDecodedQuery.forEach((el, index) => {
        const champs = el.split("=");
        newMsg[champs[0]] = champs[1];
      });

      newMsg.scat1 = null;
      newMsg.scat2 = null;
      newMsg.scat3 = null;
      newMsg.switch1 = true;
      newMsg.categorie = CAT_MODULE;
      let payload = {
        name: MODULENAME,
        data: newMsg,
        actif: false,
        dataprix: {}
      };

      this.$axios
        .post(URL_LIVREDOR_CREATE, payload)
        .then(response => {
          this.messageClient = "<strong>Merci ! </strong>Votre message est en attente de validation par l'administrateur du site";
          this.showForm = false;
          this.alertAdmin();
        })
        .catch(error => {
          console.log("error", error);
        });
    },
    alertAdmin() {
      let payload = {
        emailSender: "contact@aerialgroup.fr",
        subject: "Veuillez valider un message de votre livre d'or",
        emailsTO: [this.emailAdmin],
        emailsCC: [],
        emailsBCC: []
      };

      this.$axios
        .post(URL_EMAIL_LIVRE_ADMIN, payload)
        .then(response => {
          console.log("response", response.data);
        })
        .catch(error => {
          console.log("error", error);
        });
    },
    checkIfvalidate() {
      if (this.$route.query.params && this.$route.query.validation == "ok") {
        let decodedData = decodeURI(window.atob(this.$route.query.params));
        this.saveFromDecodedQuery(decodedData);
      }
    },
    fetchData(moduleName) {
      this.$axios
        .get(URL_LIVREDOR_ACTIV + moduleName)
        .then(response => {
          this.structureData(response.data.data);
        })
        .catch(error => {
          console.log("error", error);
        });
    },
    fetchDataConfig(name) {
      // self = this;
      this.$axios
        .get(URL_FETCH_DATA_CONFIG_SITE + name)
        .then(response => {
          let coords = JSON.parse(response.data.config.data);
          this.emailAdmin = coords[0].contenu[0].value;
        })
        .catch(error => {
          console.log("error", error);
          // this.dialogLoader = false;
        });
    },
    structureData(content) {
      content.forEach((elm, index) => {
        this.listeData.push(JSON.parse(elm.data));
      });
    },
    activForm() {
      this.showForm = !this.showForm;
    },
    checkForm() {
      this.formLivre.forEach(field => {
        if (field.value == "" && field.required == true) {
          field.error = "Ce champs est obligatoire";
          this.errorForm = true;
        }
      });
      //
    },

    submitForm() {
      this.errorForm = false;
      this.checkForm();
      console.log(process.env.VUE_APP_NODE_ENV);
      if (process.env.VUE_APP_NODE_ENV != 'developpement') {
        if (this.errorForm == false) {
          this.$axios.defaults.headers.common = {};
          this.$axios
            .get("http://jsonip.com") // service tiers
            .then(response => {
              let ipUser = response.data.ip;
              let isSpam = antiSpam(ipUser);
              if (isSpam === "spam protect") {
                this.messageClient = "Protection anti-spam. Veuillez patienter";
              }
              else {
                let formData = this.setDataForm();
                this.sendMailConsent(formData);
              }
            })
            .catch(error => {
              console.log("message", error);
            });
        }
      } else {
        let formData = this.setDataForm();
        this.sendMailConsent(formData);
      }

    },
    sendMailConsent(formData) {
      this.$axios.defaults.headers.common["siteHost"] = process.env.VUE_APP_DB_HOST;
      this.$axios.defaults.headers.common["siteDatabase"] = process.env.VUE_APP_DB_DATABASE;
      this.$axios.defaults.headers.common["siteUsername"] = process.env.VUE_APP_DB_USER;
      this.$axios.defaults.headers.common["sitePassword"] = process.env.VUE_APP_DB_PASSWORD;
      this.$axios.defaults.headers.common["siteName"] = process.env.VUE_APP_SITE_ALIAS;
      this.$axios.defaults.headers.common["siteKey"] = process.env.VUE_APP_SITE_KEY;
      // on envoi le mail pour validation du consentement avec datas clients codés
      this.globalError = "";
      const config = {
        // headers: { "Content-Type": "multipart/form-data" }
      };

      this.$axios
        .post(URL_EMAIL_LIVRE_RGPD, formData, config)
        .then(response => {
          this.messageClient = "<strong>ATTENTION ! </strong>Un email viens de vous être envoyé. <br> Merci de valider celui-ci pour finaliser l'envoi de votre message";
          this.showForm = false;
          this.formLivre.forEach((el, idx) => {
            el.value = "";
          });
          // this.handleServerErrors(response);
        })
        .catch(error => {
          console.log("message", error);
          this.globalError = error;
        });
    },
    setDataForm() {
      let emailsTO = [this.emailAdmin];
      let emailsCC = [];
      let emailsBCC = [];
      // * Notes : Avec mailtrap on est limité à 2 emails/secondes
      let emailSender = this.formLivre[1].value;
      let formData = new FormData();
      let jsonForm = JSON.stringify(this.formLivre);

      // on encode les données de l'internaute
      let stringToEncode = "titre1=" + this.formLivre[0].value + "||" + "titre2=" + this.formLivre[1].value + "||" + "switch2=" + this.formLivre[3].value + "||" + "description1=" + this.formLivre[2].value;
      // let encodedData = encodeURI(window.btoa(stringToEncode));
      let encodedData = window.btoa(stringToEncode);
      let urlToValidate = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "") + this.$route.path + "?params=" + encodedData + "&validation=ok";

      emailsTO.forEach(emailTO => {
        formData.append("emailsTO[]", emailTO);
      });

      emailsCC.forEach(emailCC => {
        formData.append("emailsCC[]", emailCC);
      });

      emailsBCC.forEach(emailBCC => {
        formData.append("emailsBCC[]", emailBCC);
      });

      formData.append("urlToValidate", encodeURI(urlToValidate));
      formData.append("emailSender", emailSender);
      formData.append("dataForm", jsonForm);
      formData.append("subject", "Veuillez valider votre message");

      return formData;
    }
  }
};
