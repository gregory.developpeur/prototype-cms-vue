const URL_API = process.env.VUE_APP_URL_DISTANT_API;
const URL_FETCH_DATA_CONFIG_SITE = URL_API + "api/config/config-from-name/";
const URL_EMAIL_CALLBACK = URL_API + "api/emails/callback";
export default {
  created: function() {
    this.fetchDataConfig("coords");
  },
  data() {
    return {
      messageClient: "",
      formSubmitedIsOK: false,
      emailAdmin: null,
      errorForm: false,
      globalError: "",

      formCallback: [
        {
          id: "champ1",
          libelle: "Téléphone",
          value: "",
          type: "tel",
          required: true,
          placeHolder: "",
          error: "",
          groupClass: "form-group",
          inputClass: "form-control",
          labelClass: ""
        },
        {
          id: "champ2",
          libelle: "Date de rappel souhaitée",
          value: "",
          type: "date",
          required: true,
          placeHolder: "",
          error: "",
          groupClass: "form-group",
          inputClass: "form-control",
          labelClass: ""
        },
        {
          id: "champ3",
          libelle: "Heure de rappel souhaitée",
          value: "",
          type: "select",
          required: true,
          placeHolder: "",
          error: "",
          groupClass: "form-group",
          inputClass: "form-control",
          labelClass: "",
          rangeMin: 7,
          rangeMax: 22
        }
      ]
    };
  },
  methods: {
    fetchDataConfig(name) {
      // self = this;
      this.$axios
        .get(URL_FETCH_DATA_CONFIG_SITE + name)
        .then(response => {
          let coords = JSON.parse(response.data.config.data);
          this.emailAdmin = coords[0].contenu[0].value;
        })
        .catch(error => {
          console.log("error", error);
          // this.dialogLoader = false;
        });
    },
    getNumbers(start, stop) {
      return new Array(stop - start).fill(start).map((n, i) => n + i);
    },
    checkForm(form) {
      form.forEach((field, index) => {
        field.error = "";
        if (field.value == "" && field.required == true) {
          field.error = "Ce champs est obligatoire";
          this.errorForm = true;
        }
        if (field.type == "tel") {
          // suppression des caractères non numériques
          const reg = new RegExp("[0-9]", "i");
          const chars = field.value.split("");
          chars.forEach((carac, idx) => {
            if (!reg.test(carac)) {
              field.error = "Veuillez remplir correctement ce champs (sans espace, sans tiret, etc...)";
              this.errorForm = true;
            }
          });

          // vérification pattern téléphone
          const regex = new RegExp(/^(0|\\+33|0033)[1-9][0-9]{8}/);
          const found = regex.test(field.value);
          if (!found) {
            field.error = "Format non valide";
            this.errorForm = true;
          }
        }
      });
    },
    submitForm() {
      this.errorForm = false;
      this.checkForm(this.formCallback);
      // return false;
      if (this.errorForm == false) {
        this.sendMailCallBack();
      }
    },
    sendMailCallBack() {
      let payload = {
        emailSender: "callback@arpaline.net",
        subject: "CALLBACK - Demande de rappel",
        emailsTO: [this.emailAdmin],
        emailsCC: [],
        emailsBCC: [],
        dataForm: JSON.stringify(this.formCallback)
      };

      this.$axios
        .post(URL_EMAIL_CALLBACK, payload)
        .then(response => {
          if (response.data.data == "Email OK") {
            this.messageClient = "La demande de rappel a été transmise. Nous vous recontacterons dans un créneau le plus proche de ce que vous avez demandé.";
            this.formSubmitedIsOK = true;
            this.formCallback.forEach(el => {
              el.value = "";
            });
          } else {
            this.messageClient = "Un problèeme serveur est survenu, la demande de rappel n'a pas pu etre transmise.";
          }
        })
        .catch(error => {
          console.log("error", error);
        });
    }
  }
};
