/* eslint-disable quotes */
/* eslint-disable space-before-function-paren */
/* eslint-disable semi */
const state = {
  authUser: null,
  adminRole: null,
  databaseUser: null,
  databasePassword: null,
  databaseHost: null,
  databaseName: null
};
const mutations = {
  SET_AUTH_USER(state, userObj) {
    state.authUser = userObj;
  },
  CLEAR_AUTH_USER(state) {
    state.authUser = null;
    state.adminRole = null;
  },
  SET_ROLE_USER(state, userObj) {
    state.adminRole = userObj;
  },
  SET_DB_USER(state) {
    state.databaseUser = process.env.VUE_APP_DB_USER;
    state.databasePassword = process.env.VUE_APP_DB_PASSWORD;
    state.databaseHost = process.env.VUE_APP_DB_HOST;
    state.databaseName = process.env.VUE_APP_DB_DATABASE;
  },
  CLEAR_DB_USER(state) {
    state.databaseUser = null;
    state.databasePassword = null;
    state.databaseHost = null;
    state.databaseName = null;
  }
};

const actions = {
  setUserObject: ({ commit }, userObj) => {
    commit("SET_AUTH_USER", userObj);
  },
  clearAuthUser: ({ commit }, userObj) => {
    commit("CLEAR_AUTH_USER");
  },
  setUserRole: ({ commit }, userObj) => {
    // console.log("state.adminRole", userObj);
    commit("SET_ROLE_USER", userObj);
  },
  setDBUser: ({ commit }) => {
    commit("SET_DB_USER");
  },
  clearDBUser: ({ commit }) => {
    commit("CLEAR_DB_USER");
  }
};
export default {
  state,
  mutations,
  actions
};
