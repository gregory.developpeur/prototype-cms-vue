/* eslint-disable quotes */
/* eslint-disable space-before-function-paren */
/* eslint-disable semi */
const state = {
  client: (localStorage.getItem("authUserClient") ? JSON.parse(localStorage.getItem("authUserClient")) : null),
  infoClient: (localStorage.getItem("infoClient") ? JSON.parse(localStorage.getItem("infoClient")) : null),
  clientId: (localStorage.getItem("clientId") ? JSON.parse(localStorage.getItem("clientId")) : null),
  isAuthenticated: (!!localStorage.getItem("authUserClient"))
}

const getters = {
  //
}

const mutations = {
  INIT_CLIENT(state, { infoClient, authUserClient }) {
    state.client = authUserClient;
    state.infoClient = JSON.parse(infoClient.data)
    state.infoClient.email = infoClient.email
    state.clientId = infoClient.id
    state.isAuthenticated = true;
    console.log("client initialized", state.client)
  },
  UPDATE_CLIENT(state, { newInfos }) {
    state.infoClient = newInfos
    localStorage.setItem("infoClient", JSON.stringify(newInfos))
  },
  UPDATE_CLIENT_LIVRAISON(state, { newInfosLivraison }) {
    state.infoClient.delivery[0] = newInfosLivraison
    let jsonClientInfos = JSON.parse(localStorage.getItem("infoClient"));
    jsonClientInfos.delivery[0] = newInfosLivraison
    localStorage.setItem("infoClient", JSON.stringify(jsonClientInfos))
  },
  LOG_OUT(state) {
    state.client = null;
    state.infoClient = null;
    state.isAuthenticated = false;
    localStorage.removeItem("authUserClient");
    localStorage.removeItem("infoClient");
    console.log("client log-out");
  }
}

const actions = {
  initClient: ({ commit }, { infoClient, authUserClient }) => {
    commit("INIT_CLIENT", { infoClient, authUserClient });
  },
  updateClient: ({ commit }, { newInfos }) => {
    commit("UPDATE_CLIENT", { newInfos })
  },
  updateClientLivraison: ({ commit }, { newInfosLivraison }) => {
    commit("UPDATE_CLIENT_LIVRAISON", { newInfosLivraison })
  },
  logout: ({ commit }) => {
    commit("LOG_OUT");
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
