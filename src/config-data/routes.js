// ======== IMPORT FRONT ========= //
import Error404 from '@/pages/p-404.vue';
import Login from '@/pages/p-login.vue';
import news from "@/pages/p-news.vue";
import livredor from "@/pages/p-livre-dor.vue";
import contact from "@/pages/p-contact.vue";
import callback from "@/pages/p-callback.vue";
import test2 from "@/pages/p-test2.vue";
import accueil from "@/pages/p-accueil.vue";
import notrecatalogue from "@/pages/p-notre-catalogue.vue";
import panier from "@/pages/p-panier.vue";
import modelesnews from "@/pages/p-modeles-news.vue";
import compteclient from "@/pages/p-compte-client.vue";
import galeriephoto from "@/pages/p-galerie-photo.vue";
import autresactualites from "@/pages/p-autres-actualites.vue";
import logos from "@/pages/p-logos.vue";
import testlogo from "@/pages/p-test-logo.vue";
// ======== IMPORT ADMIN ========= //
import AdminDefault from '@/pages/admin/p-admin-default.vue';
import AdminCategoriesListe from '@/pages/admin/categories/p-admin-categories-liste.vue';
// gestion produits
import AdminGestionProduitsListe from '@/pages/admin/gestion-produits/p-admin-gestion-produits-liste.vue';
import AdminGestionProduitsDetail from '@/pages/admin/gestion-produits/p-admin-gestion-produits-detail.vue';
import AdminGestionProduitsDetailEdit from '@/pages/admin/gestion-produits/p-admin-gestion-produits-detail-edit.vue';
// gestion clients
import AdminGestionClientsListe from '@/pages/admin/gestion-clients/p-admin-gestion-clients-liste.vue';
import AdminGestionClientsDetail from '@/pages/admin/gestion-clients/p-admin-gestion-clients-detail.vue';
import AdminGestionClientsDetailEdit from '@/pages/admin/gestion-clients/p-admin-gestion-clients-detail-edit.vue';
// users
import AdminUsersListe from '@/pages/admin/users/p-admin-users-liste.vue';
import AdminUsersCreate from '@/pages/admin/users/p-admin-users-create.vue';
import AdminUsersEdit from '@/pages/admin/users/p-admin-users-edit.vue';
// config site
import AdminConfigSite from '@/pages/admin/config-site/p-admin-config-site.vue';
// référencement
import AdminReferencement from '@/pages/admin/referencement/p-admin-referencement.vue';
// ======== ROUTES ========= //
export const routesFromBuilder = [
{ path: '/news/detail/:id', name:'news-detail', component: news},
{ path: '/news/:categorie/:scat1/:scat2/:scat3', name:'news', component: news},
{ path: '/news/:categorie/:scat1/:scat2', name:'news', component: news},
{ path: '/news/:categorie/:scat1', name:'news', component: news},
{ path: '/news/:categorie', name:'news', component: news},
{ path: '/news', name:'news', component: news},
{ path: '/livre-dor/detail/:id', name:'livre-dor-detail', component: livredor},
{ path: '/livre-dor/:categorie/:scat1/:scat2/:scat3', name:'livre-dor', component: livredor},
{ path: '/livre-dor/:categorie/:scat1/:scat2', name:'livre-dor', component: livredor},
{ path: '/livre-dor/:categorie/:scat1', name:'livre-dor', component: livredor},
{ path: '/livre-dor/:categorie', name:'livre-dor', component: livredor},
{ path: '/livre-dor', name:'livre-dor', component: livredor},
{ path: '/contact/detail/:id', name:'contact-detail', component: contact},
{ path: '/contact/:categorie/:scat1/:scat2/:scat3', name:'contact', component: contact},
{ path: '/contact/:categorie/:scat1/:scat2', name:'contact', component: contact},
{ path: '/contact/:categorie/:scat1', name:'contact', component: contact},
{ path: '/contact/:categorie', name:'contact', component: contact},
{ path: '/contact', name:'contact', component: contact},
{ path: '/callback/detail/:id', name:'callback-detail', component: callback},
{ path: '/callback/:categorie/:scat1/:scat2/:scat3', name:'callback', component: callback},
{ path: '/callback/:categorie/:scat1/:scat2', name:'callback', component: callback},
{ path: '/callback/:categorie/:scat1', name:'callback', component: callback},
{ path: '/callback/:categorie', name:'callback', component: callback},
{ path: '/callback', name:'callback', component: callback},
{ path: '/test2/detail/:id', name:'test2-detail', component: test2},
{ path: '/test2/:categorie/:scat1/:scat2/:scat3', name:'test2', component: test2},
{ path: '/test2/:categorie/:scat1/:scat2', name:'test2', component: test2},
{ path: '/test2/:categorie/:scat1', name:'test2', component: test2},
{ path: '/test2/:categorie', name:'test2', component: test2},
{ path: '/test2', name:'test2', component: test2},
{ path: '/', name:'accueil', component: accueil},
{ path: '/notre-catalogue/detail/:id', name:'notre-catalogue-detail', component: notrecatalogue},
{ path: '/notre-catalogue/:categorie/:scat1/:scat2/:scat3', name:'notre-catalogue', component: notrecatalogue},
{ path: '/notre-catalogue/:categorie/:scat1/:scat2', name:'notre-catalogue', component: notrecatalogue},
{ path: '/notre-catalogue/:categorie/:scat1', name:'notre-catalogue', component: notrecatalogue},
{ path: '/notre-catalogue/:categorie', name:'notre-catalogue', component: notrecatalogue},
{ path: '/notre-catalogue', name:'notre-catalogue', component: notrecatalogue},
{ path: '/panier/detail/:id', name:'panier-detail', component: panier},
{ path: '/panier/:categorie/:scat1/:scat2/:scat3', name:'panier', component: panier},
{ path: '/panier/:categorie/:scat1/:scat2', name:'panier', component: panier},
{ path: '/panier/:categorie/:scat1', name:'panier', component: panier},
{ path: '/panier/:categorie', name:'panier', component: panier},
{ path: '/panier', name:'panier', component: panier},
{ path: '/modeles-news/detail/:id', name:'modeles-news-detail', component: modelesnews},
{ path: '/modeles-news/:categorie/:scat1/:scat2/:scat3', name:'modeles-news', component: modelesnews},
{ path: '/modeles-news/:categorie/:scat1/:scat2', name:'modeles-news', component: modelesnews},
{ path: '/modeles-news/:categorie/:scat1', name:'modeles-news', component: modelesnews},
{ path: '/modeles-news/:categorie', name:'modeles-news', component: modelesnews},
{ path: '/modeles-news', name:'modeles-news', component: modelesnews},
{ path: '/compte-client/detail/:id', name:'compte-client-detail', component: compteclient},
{ path: '/compte-client/:categorie/:scat1/:scat2/:scat3', name:'compte-client', component: compteclient},
{ path: '/compte-client/:categorie/:scat1/:scat2', name:'compte-client', component: compteclient},
{ path: '/compte-client/:categorie/:scat1', name:'compte-client', component: compteclient},
{ path: '/compte-client/:categorie', name:'compte-client', component: compteclient},
{ path: '/compte-client', name:'compte-client', component: compteclient},
{ path: '/galerie-photo/detail/:id', name:'galerie-photo-detail', component: galeriephoto},
{ path: '/galerie-photo/:categorie/:scat1/:scat2/:scat3', name:'galerie-photo', component: galeriephoto},
{ path: '/galerie-photo/:categorie/:scat1/:scat2', name:'galerie-photo', component: galeriephoto},
{ path: '/galerie-photo/:categorie/:scat1', name:'galerie-photo', component: galeriephoto},
{ path: '/galerie-photo/:categorie', name:'galerie-photo', component: galeriephoto},
{ path: '/galerie-photo', name:'galerie-photo', component: galeriephoto},
{ path: '/autres-actualites/detail/:id', name:'autres-actualites-detail', component: autresactualites},
{ path: '/autres-actualites/:categorie/:scat1/:scat2/:scat3', name:'autres-actualites', component: autresactualites},
{ path: '/autres-actualites/:categorie/:scat1/:scat2', name:'autres-actualites', component: autresactualites},
{ path: '/autres-actualites/:categorie/:scat1', name:'autres-actualites', component: autresactualites},
{ path: '/autres-actualites/:categorie', name:'autres-actualites', component: autresactualites},
{ path: '/autres-actualites', name:'autres-actualites', component: autresactualites},
{ path: '/logos/detail/:id', name:'logos-detail', component: logos},
{ path: '/logos/:categorie/:scat1/:scat2/:scat3', name:'logos', component: logos},
{ path: '/logos/:categorie/:scat1/:scat2', name:'logos', component: logos},
{ path: '/logos/:categorie/:scat1', name:'logos', component: logos},
{ path: '/logos/:categorie', name:'logos', component: logos},
{ path: '/logos', name:'logos', component: logos},
{ path: '/test-logo/detail/:id', name:'test-logo-detail', component: testlogo},
{ path: '/test-logo/:categorie/:scat1/:scat2/:scat3', name:'test-logo', component: testlogo},
{ path: '/test-logo/:categorie/:scat1/:scat2', name:'test-logo', component: testlogo},
{ path: '/test-logo/:categorie/:scat1', name:'test-logo', component: testlogo},
{ path: '/test-logo/:categorie', name:'test-logo', component: testlogo},
{ path: '/test-logo', name:'test-logo', component: testlogo},
{ path: '/login', name: 'login', component: Login },
{ path: '/admin', name: 'admin-default', component: AdminDefault,  meta: { requiresAuth: true }, children: [
  { path: 'gestion-produits/:module', name: 'admin-gestion-produits-liste', component: AdminGestionProduitsListe, meta: { requiresAuth: true }, props: true },
  { path: 'gestion-produits/:module/create', name: 'admin-gestion-produits-detail', component: AdminGestionProduitsDetail, meta: { requiresAuth: true } },
  { path: 'gestion-produits/:module/edit/:id', name: 'admin-gestion-produits-detail-edit', component: AdminGestionProduitsDetailEdit, meta: { requiresAuth: true } },
  { path: 'gestion-clients/:module', name: 'admin-gestion-clients-liste', component: AdminGestionClientsListe, meta: { requiresAuth: true }, props: true },
  { path: 'gestion-clients/:module/create', name: 'admin-gestion-clients-detail', component: AdminGestionClientsDetail, meta: { requiresAuth: true } },
  { path: 'gestion-clients/:module/edit/:id', name: 'admin-gestion-clients-detail-edit', component: AdminGestionClientsDetailEdit, meta: { requiresAuth: true } },
  { path: 'categories', name: 'admin-categories-liste', component: AdminCategoriesListe, meta: { requiresAuth: true } },
  { path: 'users', name: 'admin-users-liste', component: AdminUsersListe, meta: { requiresAuth: true } },
  { path: 'users/create', name: 'admin-users-create', component: AdminUsersCreate, meta: { requiresAuth: true } },
  { path: 'users/edit/:id', name: 'admin-users-edit', component: AdminUsersEdit, meta: { requiresAuth: true } },
  { path: '/config-site', name: 'admin-config-site', component: AdminConfigSite, meta: { requiresAuth: true } },
  { path: '/referencement', name: 'admin-referencement', component: AdminReferencement, meta: { requiresAuth: true } }
]},
{ path: '/404', name: 'error404', component: Error404 }
];
