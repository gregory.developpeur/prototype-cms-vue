const URL_API = process.env.VUE_APP_URL_DISTANT_API;
const URL_EMAIL_CONTACT_UPLOAD = URL_API + "api/emails/contact-upload";

export const sendMailMixin = {
  data() {
    return {
      globalError: "",
      formSubmitedIsOK: false
    };
  },
  methods: {
    hello() {
      console.log("test from mixin!");
    },
    sendMail(formData) {
      this.globalError = "";
      const config = {
        headers: { "Content-Type": "multipart/form-data" }
      };

      this.$axios
        .post(URL_EMAIL_CONTACT_UPLOAD, formData, config)
        .then(response => {
          console.log("response.data", response.data);
          this.handleServerErrors(response);
        })
        .catch(error => {
          console.log("message", error);
          this.globalError = error;
        });
    },
    handleServerErrors(response) {
      console.log("handleServerErrors");
      if (response.data.data == "Email OK") {
        this.formSubmitedIsOK = true;
      }

      if (response.data.message) {
        let errorMsg = [];
        for (var i in response.data.message) {
          errorMsg.push([i, response.data.message[i]]);
        }
        this.globalError = errorMsg[0][1][0];
      }
    }
  }
};
