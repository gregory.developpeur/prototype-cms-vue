import firebase from 'firebase'
import 'firebase/storage'

var firebaseConfig = {
  apiKey: 'AIzaSyDpc0cdbmmJ9hlrNb6VIWmR7XTsrv3SY8M',
  authDomain: 'medias-new-archi-dev.firebaseapp.com',
  databaseURL: 'https://medias-new-archi-dev.firebaseio.com',
  projectId: 'medias-new-archi-dev',
  storageBucket: 'medias-new-archi-dev.appspot.com',
  messagingSenderId: '483284385346',
  appId: '1:483284385346:web:06f445441b88a1a7'
}
// Initialize Firebase
export const fb = firebase.initializeApp(firebaseConfig)
